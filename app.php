<?php

declare(strict_types=1);

require_once __DIR__ . '/vendor/autoload.php';

function my_dd($var) { dump(is_iterable($var) ? iterator_to_array($var) : $var); }

/*********************************
 * Listar bd y colecciones
 ********************************/
function dbList(MongoDB\Client $client)
{
    my_dd($client->listDatabaseNames());
    $db = $client->selectDatabase('pruebas');
    my_dd($db->listCollectionNames());
}

/*********************************
 * Finds
 ********************************/
function finds(MongoDB\Client $client)
{
    $db = $client->selectDatabase('pruebas');
    $collection = $db->selectCollection('productos');

    // query
    // project
    // limit

    $results = $collection->find(
        [
            '$or' => [
                ['name' => 'Product 1'],
                ['extra.tags' => 'tag1']
            ]
        ],
        [
//            'limit' => 1,
//            'projection' => ['active' => 1],
//            'sort' => ['active' => -1],
        ],
    );
    my_dd($results);
}

/*********************************
 * Inserts
 ********************************/
function inserts(MongoDB\Client $client)
{
    $db = $client->selectDatabase('pruebas');
    $collection = $db->selectCollection('productos');

    $results = $collection->insertOne(
        [
            //'_id' => 'myproducto',
            'name' => 'Product 1',
            'description' => 'sfasfasfsd',
            'price' => 25.82,
            'active' => true,
            'extra' => [
                'category' => 'Category 1',
                'tags' => ['tag1', 'tag2']
            ],
            // Esto realmente tiene su sistema "propio"
            'created_at' => (new DateTime())->format(DateTime::ATOM),
        ]
    );
    my_dd($results);
}

/*********************************
 * Updates
 ********************************/
function updates(MongoDB\Client $client)
{
    $db = $client->selectDatabase('pruebas');
    $collection = $db->selectCollection('productos');

    $results = $collection->updateOne(
        [
            '_id' => new \MongoDB\BSON\ObjectId('6246b90015108c372510c032'),
        ],
        [
            '$set' => [
                'active' => true,
            ],
        ],
        [
            'upsert' => true,
        ]
    );
    my_dd($results);
}

/*********************************
 * Deletes
 ********************************/
function deletes(MongoDB\Client $client)
{
    $db = $client->selectDatabase('pruebas');
    $collection = $db->selectCollection('productos');

    $results = $collection->deleteMany(
        [
//            'active' => false,
        ],
    );
    my_dd($results);
}

/*********************************
 * Configuración e inicio
 ********************************/

$uri = 'mongodb://test_user:test_pass@localhost:27017/?authSource=admin&readPreference=primary&ssl=false';

$options = [
    'typeMap' => [
        'root' => 'array', 'document' => 'array', 'array' => 'array'
    ]
];

$client = new MongoDB\Client($uri, [], $options);

dbList($client);
//inserts($client);
//finds($client);
//updates($client);
//deletes($client);

exit;